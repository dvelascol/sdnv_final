#!/bin/bash

VNF2="mn.dc1_vcpe-1-2-vyos-router-1"


docker exec -ti $VNF2 /bin/bash -c "
source /opt/vyatta/etc/functions/script-template
configure
# tunel vxlan0
set interfaces vxlan vxlan0 address 192.168.255.1/24
set interfaces vxlan vxlan0 remote 192.168.100.3
set interfaces vxlan vxlan0 vni 1
set interfaces vxlan vxlan0 port 4789
set interfaces vxlan vxlan0 mtu 1400

#default gateway
set interfaces ethernet eth2 address 10.2.3.1/24
delete protocols static route 0.0.0.0/0
delete protocols static route 10.2.2.0/24
set protocols static route 0.0.0.0/0 next-hop 10.2.3.254
set protocols static route 10.2.2.0/24 next-hop 10.2.3.254

#ssh - no es necesario
set service ssh port 22

#dhcp
set service dhcp-server shared-network-name vxlan0 authoritative
set service dhcp-server shared-network-name vxlan0 subnet 192.168.255.0/24 default-router '192.168.255.1'
set service dhcp-server shared-network-name vxlan0 subnet 192.168.255.0/24 domain-name 'example.org'
set service dhcp-server shared-network-name vxlan0 subnet 192.168.255.0/24 lease '7200'
set service dhcp-server shared-network-name vxlan0 subnet 192.168.255.0/24 range 0 start '192.168.255.20'
set service dhcp-server shared-network-name vxlan0 subnet 192.168.255.0/24 range 0 stop '192.168.255.30'

#dns

set service dns forwarding cache-size '0'
set service dns forwarding listen-address '192.168.255.1'
set service dns forwarding allow-from '192.168.255.0/24'

#nat
#set nat source rule 120 source address 192.168.255.0/24
#set nat source rule 120 outbound-interface eth2
#set nat source rule 120 translation address 100.64.0.10-100.64.0.20
set nat source rule 1 source address 192.168.255.0/24
set nat source rule 1 outbound-interface eth2
set nat source rule 1 translation address 'masquerade'

commit
save
exit
"

