#!/bin/bash

#Onboarding de VNFs
osm vnfd-create /home/upm/Desktop/osm_vclass.tar.gz
sleep 5
osm vnfd-create /home/upm/Desktop/osm_vcpe-vy.tar.gz
sleep 5
#Onboarding de NS
osm nsd-create /home/upm/Desktop/osm_vCPE.tar.gz
sleep 5
#Instanciacion NS
osm ns-create --ns_name vcpe-1 --nsd_name vCPE --vim_account emu-vim
#osm ns-create --ns_name vcpe-2--nsd_name vCPE --vim_account emu-vim
